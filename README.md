# headlines-api

JSON file of aggregated top headlines that is consistently updated (programatically) every 20 minutes. 

### <ins>__Base URL__</ins>
`https://headlines.recon.us.com`

### <ins>__JSON Endpoint__</ins>
`/headlines.json`

### <ins>__JSON Endpoint (CORS-Friendly)__</ins>
> + Header: Access-Control-Allow-Origin: *
> + Header: Content-type: application/json

`/headlines.php`

---
### [<ins>__Documentation & Usage Example__</ins>](https://headlines.recon.us.com)

![](https://i.ibb.co/yq2Q7Vd/ezgif-6-46b4d3dfbf3c.gif)

### <ins>Updates</ins>

- #### December 23rd, 2021 
The engine is updating again! 
API endpoint is back at [headlines.recon.us.com](https://headlines.recon.us.com/headlines.json)

- #### November 15th, 2021 
Added a bunch of news sources, with emphasis on global (non-US) news sources.
